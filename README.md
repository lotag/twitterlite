# Twitter Lite

## Directory Structure
The directory Structure is as follows

```
run.py - mainfile invoked when server is run
twittersite/
	__init__.py -run to initialise the application  
	models.py   -functionality and classes defined here  
	views.py    -app routes to pages defined here  
	static/  
		stylesheets and assets go here  
	templates/  
		html goes here  
```

##Git Usage
Final production ready code will reside on the master branch and new features will be merged into the dev branch on which it will be tested before merging it into the master branch. There will be no pushing to dev and master, only merge requests will be accepted.

Create a new branch for each feature being developed an merge it into dev on completion of the feature.

Please ensure that your local dev branch is up to date by pulling from remote before creating a new branch from dev.

##Trello Usage
Trello is used to keep track of who is working on what part of the project. When starting work on a new feature please add yourself to the card and also tick of the requirements of that card as you go along.

##Database
A database user with the following credentials has been added in the code
```
username: lotag
password: lotag
```
Please create a user with those credentials in your local database if you would like the application to be able to interface with the database on your machine.

In terms of nodes we have the following
```
Person: username
	password
	profile photo
	session ID

Tweet: 	content
	time

Hashtag:name
```
We also have the following links between nodes
```
Person--[follow]-->Person
Person--[like]---->Tweet
Person--[write]--->Tweet
Person--[retweet]->Tweet
Hashtag-[tag]----->Tweet
```
