from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length, EqualTo, Regexp

class ResetForm(FlaskForm):
    regexp_message = 'Password requires at least 1 digit, 1 upper case letter and 1 lower case letter and be of minimum length 8.'
    password = PasswordField('Password', validators=[DataRequired(), Regexp('^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,}$', flags=0, message=regexp_message)])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Submit')