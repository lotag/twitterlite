from flask import Flask, render_template, request, redirect, url_for, make_response, session, flash, escape
from .models import *
from wtforms import Form, TextField
from werkzeug.utils import secure_filename
import os
from os.path import join, dirname, realpath
from .resetpass import ResetForm
from .login import LoginForm
from .register import RegistrationForm

UPLOAD_FOLDER = join(dirname(realpath(__file__)), 'static/IMG/')
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = 'asefuhnuaguefgvnywsngvieyswnt'


@app.before_request
def make_session_permanent():
    session.permanent = True

@app.route("/")
@app.route("/home")
def home():
    if request.cookies.get("USERSESSION") != "" and request.cookies.get("USERSESSION") != None:
        myuser = getUserBySession(request.cookies.get('USERSESSION'))
        myuser = myuser.data()[0]['n']
        posts=getFeed(request.cookies.get("USERSESSION"))
        return render_template("index.html", login=True, pageName="TwitterLite", me=myuser, posts=posts)
    else:
        return render_template("index.html", login=False, pageName="TwitterLite")


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        registerUser(form.username.data, form.password.data)
        login_result = loginUser(form.username.data, form.password.data)
        if login_result is not None:
            resp = make_response(redirect(url_for('home')))
            resp.set_cookie(
                'USERSESSION', generateSessionID(form.username.data))
            return resp
    return render_template("register.html", form = form, pageName="TwitterLite - Register")


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        login_result = loginUser(form.username.data, form.password.data)
        if login_result is not None:
            resp = make_response(redirect(url_for('home')))
            resp.set_cookie('USERSESSION', login_result['p.sessionID'])
            return resp
        flash("Incorrect Username or Password")
    return render_template("login.html", form=form, pageName="TwitterLite - Login")


@app.route("/logout", methods=['GET', 'POST'])
def logout():
    resp = make_response(redirect(url_for('home')))
    resp.set_cookie('USERSESSION', '')
    return resp


@app.route("/profile")
def profile():
    session_id = request.cookies.get('USERSESSION')
    myuser = getUserBySession(session_id)
    for user in myuser:
        myuser=user['n']
    return render_template("profile.html", login=True, pageName="TwitterLite - Profile", user=myuser, posts=getUserTweets(myuser['username']), retweets=getUserRetweets(myuser['username']), following=getUserFollowing(myuser['username']), followers=getUserFollowers(myuser['username']), suggestions=getSuggestPeople(myuser['username']))

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/api/uploadPhoto",  methods=['POST'])
def upload_photo():
    if (request.method == 'POST'):
        if ('file' not in request.files):
            flash('No file selected')
            return redirect(url_for('profile'))
        file = request.files['file']
        if (file.filename == ''):
            flash('No file selected')
            return redirect(url_for('profile'))
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            session_id = request.cookies.get('USERSESSION')
            updatePhoto(session_id, 'IMG/'+filename)
            return redirect(url_for('profile'))
    return redirect(url_for('profile'))


@app.route("/updateBio", methods=['GET', 'POST'])
def updateBio():
    session_id = request.cookies.get('USERSESSION')
    if (request.method == 'POST'):
        bio = request.form['bio']
        updateBioDB(session_id, bio)
        return redirect(url_for('profile'))
    return render_template("updateBio.html", login=True,pageName="Update Bio")


@app.route("/resetpwd", methods=['GET', 'POST'])
def resetpwd():
    session_id = request.cookies.get('USERSESSION')
    form = ResetForm()
    if form.validate_on_submit():
        resetpwdDB(session_id, form.password.data)
        return redirect(url_for('profile'))
    return render_template("resetPassword.html",  login=True, pageName="Reset Password", form=form)


@app.route("/search", methods=['GET', 'POST'])
def search():
    myuser = getUserBySession(request.cookies.get('USERSESSION'))
    myuser = myuser.data()[0]['n']
    if (request.method == 'POST'):
        phrase = request.form['phrase']
        data = searchUsers(phrase, myuser['username'])
        udata = []
        for d in data:
            udata.append(d['n'])
        return render_template("search.html", login=True, pageName="TwitterLite - Search", users=udata, me=myuser)
    return render_template("search.html", login=True, pageName="TwitterLite - Search", me=myuser)


@app.route("/visualize")
def visualize():
    userlikes = countLikes()
    return render_template("visualize.html", pageName="TwitterLite - Visualize", userdict=userlikes)


@app.route("/api/follow/")
def follow():
    if (request.method == 'GET'):
        user1 = request.args.get('user1')
        user2 = request.args.get('user2')
        followUser(user1, user2)
        return 'success'
    return 'error'

@app.route("/api/retweet/")
def retweet():
    if (request.method == 'GET'):
        user = request.args.get('user')
        time = request.args.get('time')
        reTweet(user, time)
        return redirect(url_for('home'))
    return 'error'

@app.route("/api/like/")
def like():
    if (request.method == 'GET'):
        user = request.args.get('user')
        time = request.args.get('time')
        tweetLike(user, time)
        return redirect(url_for('home'))
    return 'error'

@app.route("/hashtag")
def hashtagTweets():
    """This endpoint should be called with the name of the hashtag(not including the #)"""
    if (request.method == 'GET'):
        hashtag = request.args.get('tag')
        data = getTweetsByHashtags(hashtag)
        print(data)
        return render_template("hashtag.html", login=True, pageName="TwitterLite - Hashtag", tag=hashtag, tweets=data)
    return 'error'

@app.route("/trending")
def trending():
    tags = getTrendingHashtags()
    return render_template("trending.html", login=True, pageName="TwitterLite - Trending", tags=tags)

@app.route("/tweets", methods=['GET', 'POST'])
def tweets():
    user = getUserBySession(request.cookies.get('USERSESSION'))
    user = user.data()[0]['n']
    if (request.method == 'POST'):
        contents = request.form['contents']
        contents = contents.replace("\'", "")
        contents = contents.replace("\"", "")
        createTweet(user['username'], contents)
        hashtag = request.form['hashtag']
        hashtag = hashtag.replace(" ", "")
        tags = hashtag.split('#')
        for tag in tags:
            if (tag!=""):
                tagHashtag(tag, contents)
        return redirect(url_for("home"))
    return render_template("tweet.html", login=True, pageName="TwitterLite - Tweets")
        