from py2neo import Graph, Node
import os
import time
import random
import hashlib
import datetime
import re

username = "lotag"
password = "b.ID0ySSS021Eo.eiaMmI2SmjJkE9ZD"

graph = Graph('https://hobby-cjkmnjmgjfpbgbkefccffccl.dbs.graphenedb.com:24780/db/data/',
              bolt=False, username=username, password=password)

def getFeed(user):
    data = graph.run("MATCH (p1:Person {sessionID:'" + user + "'})-[:follow]->(p2:Person)-[:write]->(tw:Tweet) "
                    +"OPTIONAL MATCH (h:Hashtag)-[:tag]->(tw) "
                    +"RETURN p2, tw, COLLECT(h) as hashtags, SIZE((:Person)-[:like]->(tw)) AS likes, SIZE((:Person)-[:retweet]->(tw)) AS retweets "
                    +"ORDER BY tw.time DESC, likes DESC")
    returnData = []
    for d in data:
        returnData.append({'user': d['p2'], 'date': datetime.datetime.fromtimestamp(int(
            d['tw']['time'])//1000).date(), 'tweet': d['tw'],
             'hashtags': d['hashtags'], 'likes': d['likes'], 'retweets': d['retweets']})
    return returnData

# Registers a new user:


def registerUser(user, passw):
    session_ID = generateSessionID(user)
    person = Node("Person", username=user, password=hashlib.md5(passw.encode()).hexdigest(
    ), photo="IMG/profile_default.jpg", bio="This is me", sessionID=session_ID)
    graph.create(person)

# Returns the sessionID if login is successful


def loginUser(user, passw):
    enc_password = hashlib.md5(passw.encode()).hexdigest()
    result = graph.run("MATCH (p:Person {username:{U}, password:{P}}) RETURN p.sessionID", {
                       "U": user, "P": enc_password})
    for record in result:
        return record
    return None


# Generates a sessionID
def generateSessionID(user):
    return "{}{}".format(user, hashlib.md5(password.encode()).hexdigest())


def searchUsers(phrase, myname):
    data = graph.run("MATCH (n:Person) where n.username CONTAINS \"" +
                     phrase+"\" AND NOT n.username = \""+myname+"\" AND NOT(: Person {username: \"" +
                     myname+"\"})-[:follow]->(n) return n")
    return data


def followUser(user1, user2):
    graph.run("MATCH (a:Person),(b:Person) WHERE a.username = \""+user1 +
              "\" AND b.username = \""+user2+"\" CREATE (a)-[r:follow]->(b)")


def getUserBySession(sessionID):
    data = graph.run(
        "MATCH (n:Person) WHERE n.sessionID = \""+sessionID+"\" return n")
    return data


def countLikes():
    users = graph.run("MATCH (m:Person) return m")
    usersList = []
    likesList = []
    for user in users:
        usersList.append(user['m']['username'])
    data = graph.run(
        "MATCH (n:Person)-[:write]->(:Tweet)<-[l:like]-(m:Person) return n, count(l)")
    for user in data:
        name = user['n']['username']
        for person in usersList:
            if (person == name):
                likes = user['count(l)']
                likesList.append(likes)
            else:
                likesList.append(0)

    dictionary = dict(zip(usersList, likesList))
    return dictionary


def updatePhoto(sessionID, photoName):
    graph.run('MATCH (p:Person) WHERE p.sessionID=\'' +
              sessionID+'\' set p.photo=\''+photoName+'\'')


def updateBioDB(sessionID, bio):
    bio = re.sub('[\'\"]', '', bio)
    graph.run('MATCH (p:Person) WHERE p.sessionID=\"' +
              sessionID+'\" set p.bio=\"'+bio+'\"')


def resetpwdDB(sessionID, pwd):
    enc_password = hashlib.md5(pwd.encode()).hexdigest()
    graph.run('MATCH (p:Person) WHERE p.sessionID=\'' +
              sessionID+'\' set p.password=\''+enc_password+'\'')

# Get a list of a user's tweets


def getUserTweets(user):
    data = graph.run(
        "MATCH (a:Person {username: '" + user + "'})-[:write]->(n) RETURN n ORDER BY n.time")
    posts = data.data()

    # Add some data of our own
    for post in posts:
        # Put in some user userdata
        poster = graph.run(
            "MATCH (a:Tweet)<-[:write]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['user'] = poster.data()[0]['n']

        # Collect hashtags
        hashtags = graph.run(
            "MATCH (a:Tweet)<-[:tag]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['hashtags'] = hashtags.data()

        # Collect likes
        likes = graph.run(
            "MATCH (a:Tweet)<-[:like]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['likes'] = likes.data()

        # Collect retweets
        likes = graph.run(
            "MATCH (a:Tweet)<-[:retweet]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['retweets'] = likes.data()

        # And fix the time
        post['n']['time'] = datetime.datetime.fromtimestamp(int(post['n']['time'])//1000).date()

    return posts

# Get a list of a user's retweets
def getUserRetweets(user):
    data = graph.run(
        "MATCH (a:Person {username: '" + user + "'})-[:retweet]->(n) RETURN n ORDER BY n.time")
    posts = data.data()

    # Add some data of our own
    for post in posts:
        # Put in some user userdata
        poster = graph.run(
            "MATCH (a:Tweet)<-[:write]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['user'] = poster.data()[0]['n']

        # Collect hashtags
        hashtags = graph.run(
            "MATCH (a:Tweet)<-[:tag]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['hashtags'] = hashtags.data()

        # Collect likes
        likes = graph.run(
            "MATCH (a:Tweet)<-[:like]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['likes'] = likes.data()

        # Collect retweets
        likes = graph.run(
            "MATCH (a:Tweet)<-[:retweet]-(n) WHERE a.content = '" + post['n']['content'] + "' AND a.time = " + str(post['n']['time']) + " RETURN n")
        post['n']['retweets'] = likes.data()

        # And fix the time
        post['n']['time'] = datetime.datetime.fromtimestamp(int(post['n']['time'])//1000).date()

    return posts

# Get a list of a user's followers


def getUserFollowers(user):
    data = graph.run(
        "MATCH (a:Person {username: '" + user + "'})<-[:follow]-(n) RETURN n")
    return data.data()

# Get a list of people that the user is following


def getUserFollowing(user):
    data = graph.run(
        "MATCH (a:Person {username: '" + user + "'})-[:follow]->(n) RETURN n")
    return data.data()

# get all the tweets with a particular hashtag
def getTweetsByHashtags(hashtag):
    data = graph.run("match (h:Hashtag)-[p:tag]->(t:Tweet)<-[:write]-(a:Person) where h.name=\""+hashtag +
                     "\" optional match (t)<-[l:retweet]-(:Person) with a, t, h, count(l) as retweets optional match (t)<-[r:like]-(:Person) return a, t, h,retweets, count(r) as likes")
    returnData = []
    for d in data:
        returnData.append({'user': d['a']['username'], 'date': datetime.datetime.fromtimestamp(int(
            d['t']['time'])//1000).date(), 'tweet': d['t']['content'], 'likes': d['likes'], 'retweets': d['retweets']})
    return returnData

def getTrendingHashtags():
    data = graph.run("MATCH (h:Hashtag) OPTIONAL MATCH (h)-[t:tag]->(:Tweet) WITH h, count(t) as postcount OPTIONAL MATCH (h)-[:tag]->(:Tweet)<-[l:like]-(:Person) return h, count(l) as likecount, postcount ORDER BY likecount DESC, postcount DESC LIMIT 5")
    returnData = []
    for d in data:
        returnData.append(d['h']['name'])
    return returnData

def getSuggestPeople(user):
    data = graph.run("MATCH (p1:Person {username:'" + user + "'})-[:follow]->(p2:Person), "
                    +"      (p2)-[:follow]->(p3:Person) "
                    +"WHERE NOT p3.username = '" + user + "' AND NOT ((p1)-[:follow]->(p3)) "
                    +"OPTIONAL MATCH (p3)-[:write]->(tw:Tweet), "
                    +"      (:Person)-[l:like]->(tw) "
                    +"RETURN p3, COUNT(l) AS total_likes, COUNT(Distinct tw) AS total_tweets "
                    +"ORDER BY total_likes DESC, total_tweets DESC "
                    +"LIMIT 10")
    return data

def reTweet(user, time):
    graph.run("MATCH (a:Person {username: '" + user + "'}), (b:Tweet) WHERE b.time="+str(time)+" CREATE UNIQUE (a)-[:retweet]->(b)")

def tweetLike(user, time):
    graph.run("MATCH (a:Person {username: '" + user + "'}), (b:Tweet) WHERE b.time="+str(time)+" CREATE UNIQUE (a)-[:like]->(b)")
    
# Create a new tweet
def createTweet(user, content):
    graph.run("MATCH (a:Person {username: '" + user + "'}) create (a)-[:write]->(:Tweet {content: '" + content + "', time:timestamp()})")

# Create a hashtag
def tagHashtag(tag, contents):
    graph.run("MATCH(a:Tweet {content: \'"+contents+"\'}) MERGE (t:Hashtag {name: \'"+tag+"\'}) merge (t)-[:tag]->(a)")


